from PySide2.QtCore import Signal, QObject
import sys, os, subprocess

class Ph2_ACF_Interface(QObject):

    update = Signal ( object , object )

    def __init__( self , pConfig ):
        super(Ph2_ACF_Interface, self).__init__(  )
        self.ph2AcfFolder   = "./Ph2_ACF/"
        self.config = pConfig
        #self.setEnvironmentVariablesForPh2ACF()
        self.importPh2ACF()

    def setEnvironmentVariablesForPh2ACF(self):
        #get environment variables , source Ph2_ACF, get environment variables again. Everything in one subprocess
        try:
            check = subprocess.Popen('printenv && source ./setup.sh && printenv' ,stdout=subprocess.PIPE, stderr=subprocess.PIPE, executable="/bin/bash", shell=True, cwd="./Ph2_ACF/") 
        except FileNotFoundError:
            print("GIPHT:\tPh2_ACF folder not exist!")
            return
        #Get all variables and count how much they are present in the list
        env_variables = check.communicate()[0].splitlines()
        #Split the entries and set them for the parent process
        #Skip the BASH_FUNC blocks
        for entry in env_variables:
            if entry.decode().startswith(' ') or entry.decode().startswith('}') or entry.decode().startswith('BASH_FUNC'):
                continue
            #print(entry.decode())
            try:
                splitEntry = entry.decode().split('=')
                os.environ[splitEntry[0]] = splitEntry[1]
            except:
                continue

        #sys.path.insert(1, os.getenv('PH2ACF_BASE_DIR'))
        print("GIPHT:\tEnvironment variables for Ph2_ACF set")

    def importPh2ACF( self ):
        try:
            sys.path.insert(1, os.getenv('PH2ACF_BASE_DIR'))
            import lib.Ph2_ACF_PythonInterface as Ph2_ACF
            import pythonUtils.Ph2_ACF_StateMachine as Ph2_ACF_StateMachine
            Ph2_ACF.configureLogger(os.getenv('PH2ACF_BASE_DIR') + "/settings/logger.conf")
            self.sm = Ph2_ACF_StateMachine.StateMachine()
            return True
        except ModuleNotFoundError:
            print("Ph2_ACF not reachable: ModuleNotFoundError. Check python version for compilation of pybind11 package")
            return False

    def listFirmware( self ):
        try:
            if self.importPh2ACF():
                firmwareList = self.sm.listFirmware(self.config,0)
                for firmware in firmwareList:
                    self.update.emit("data","Firmware>"+firmware)
        except:
            print("FW list empty")

    def turnOffVTRXLight(self):
        #Resultsdirectory must be added in python ph2acf
        if self.importPh2ACF():
            calibrationname = "vtrxoff"

            self.sm.setCalibrationName(calibrationname)
            self.sm.setConfigurationFiles(self.config)

            self.sm.runCalibration()
        else:
            return

    def moduleTest(self ):
        #Resultsdirectory must be added in python ph2acf
        if self.importPh2ACF():

            calibrationname = "calibrationandpedenoise"

            self.sm.setCalibrationName(calibrationname)
            self.sm.setConfigurationFiles(self.config)

            os.environ['GIPHT_RESULT_FOLDER'] = "./Ph2_ACF/Results/"
            self.sm.setRunNumber(0)
            self.sm.runCalibration()
            print(self.sm.getErrorMessage())

        else:
            return

if __name__ == "__main__":

    ph2 = Ph2_ACF_Interface(sys.argv[1])
    ph2.listFirmware()
    ph2.turnOffVTRXLight()