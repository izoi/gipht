# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'PowerSupplyWidget.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_PowerSupplyWidget(object):
    def setupUi(self, PowerSupplyWidget):
        if not PowerSupplyWidget.objectName():
            PowerSupplyWidget.setObjectName(u"PowerSupplyWidget")
        PowerSupplyWidget.resize(581, 747)
        self.gridLayoutWidget_2 = QWidget(PowerSupplyWidget)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(10, 10, 559, 721))
        self.gridLayout_2 = QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.connectionComboBox = QComboBox(self.gridLayoutWidget_2)
        self.connectionComboBox.addItem("")
        self.connectionComboBox.addItem("")
        self.connectionComboBox.setObjectName(u"connectionComboBox")

        self.gridLayout_2.addWidget(self.connectionComboBox, 12, 0, 1, 1)

        self.connectionLayout = QVBoxLayout()
        self.connectionLayout.setObjectName(u"connectionLayout")

        self.gridLayout_2.addLayout(self.connectionLayout, 10, 0, 2, 3)

        self.spacer = QLabel(self.gridLayoutWidget_2)
        self.spacer.setObjectName(u"spacer")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spacer.sizePolicy().hasHeightForWidth())
        self.spacer.setSizePolicy(sizePolicy)

        self.gridLayout_2.addWidget(self.spacer, 6, 0, 1, 1)

        self.checkButton = QPushButton(self.gridLayoutWidget_2)
        self.checkButton.setObjectName(u"checkButton")

        self.gridLayout_2.addWidget(self.checkButton, 1, 1, 1, 1)

        self.launchServerCheckBox = QCheckBox(self.gridLayoutWidget_2)
        self.launchServerCheckBox.setObjectName(u"launchServerCheckBox")

        self.gridLayout_2.addWidget(self.launchServerCheckBox, 4, 0, 1, 1)

        self.seriesComboBox = QComboBox(self.gridLayoutWidget_2)
        self.seriesComboBox.setObjectName(u"seriesComboBox")

        self.gridLayout_2.addWidget(self.seriesComboBox, 2, 2, 1, 1)

        self.closeButton = QPushButton(self.gridLayoutWidget_2)
        self.closeButton.setObjectName(u"closeButton")

        self.gridLayout_2.addWidget(self.closeButton, 1, 0, 1, 1)

        self.index = QLabel(self.gridLayoutWidget_2)
        self.index.setObjectName(u"index")
        sizePolicy.setHeightForWidth(self.index.sizePolicy().hasHeightForWidth())
        self.index.setSizePolicy(sizePolicy)
        self.index.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.index, 0, 1, 1, 1)

        self.channelTreeWidget = QTreeWidget(self.gridLayoutWidget_2)
        __qtreewidgetitem = QTreeWidgetItem()
        __qtreewidgetitem.setText(0, u"1");
        self.channelTreeWidget.setHeaderItem(__qtreewidgetitem)
        self.channelTreeWidget.setObjectName(u"channelTreeWidget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.channelTreeWidget.sizePolicy().hasHeightForWidth())
        self.channelTreeWidget.setSizePolicy(sizePolicy1)

        self.gridLayout_2.addWidget(self.channelTreeWidget, 13, 0, 1, 3)

        self.idLabel = QLabel(self.gridLayoutWidget_2)
        self.idLabel.setObjectName(u"idLabel")
        sizePolicy.setHeightForWidth(self.idLabel.sizePolicy().hasHeightForWidth())
        self.idLabel.setSizePolicy(sizePolicy)

        self.gridLayout_2.addWidget(self.idLabel, 3, 0, 1, 1)

        self.hvlvSlot = QHBoxLayout()
        self.hvlvSlot.setObjectName(u"hvlvSlot")
        self.hvSlotLabel = QLabel(self.gridLayoutWidget_2)
        self.hvSlotLabel.setObjectName(u"hvSlotLabel")
        sizePolicy.setHeightForWidth(self.hvSlotLabel.sizePolicy().hasHeightForWidth())
        self.hvSlotLabel.setSizePolicy(sizePolicy)

        self.hvlvSlot.addWidget(self.hvSlotLabel)

        self.hvSlotComboBox = QComboBox(self.gridLayoutWidget_2)
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.addItem("")
        self.hvSlotComboBox.setObjectName(u"hvSlotComboBox")
        sizePolicy.setHeightForWidth(self.hvSlotComboBox.sizePolicy().hasHeightForWidth())
        self.hvSlotComboBox.setSizePolicy(sizePolicy)

        self.hvlvSlot.addWidget(self.hvSlotComboBox)

        self.lvSlotLabel = QLabel(self.gridLayoutWidget_2)
        self.lvSlotLabel.setObjectName(u"lvSlotLabel")
        sizePolicy.setHeightForWidth(self.lvSlotLabel.sizePolicy().hasHeightForWidth())
        self.lvSlotLabel.setSizePolicy(sizePolicy)

        self.hvlvSlot.addWidget(self.lvSlotLabel)

        self.lvSlotComboBox = QComboBox(self.gridLayoutWidget_2)
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.addItem("")
        self.lvSlotComboBox.setObjectName(u"lvSlotComboBox")

        self.hvlvSlot.addWidget(self.lvSlotComboBox)


        self.gridLayout_2.addLayout(self.hvlvSlot, 6, 1, 1, 2)

        self.saveButton = QPushButton(self.gridLayoutWidget_2)
        self.saveButton.setObjectName(u"saveButton")

        self.gridLayout_2.addWidget(self.saveButton, 1, 2, 1, 1)

        self.label_3 = QLabel(self.gridLayoutWidget_2)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_2.addWidget(self.label_3, 2, 0, 1, 1)

        self.indexLabel = QLabel(self.gridLayoutWidget_2)
        self.indexLabel.setObjectName(u"indexLabel")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.indexLabel.sizePolicy().hasHeightForWidth())
        self.indexLabel.setSizePolicy(sizePolicy2)

        self.gridLayout_2.addWidget(self.indexLabel, 0, 2, 1, 1)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.addLVChannelButton = QPushButton(self.gridLayoutWidget_2)
        self.addLVChannelButton.setObjectName(u"addLVChannelButton")

        self.horizontalLayout.addWidget(self.addLVChannelButton)

        self.addHVChannelButton = QPushButton(self.gridLayoutWidget_2)
        self.addHVChannelButton.setObjectName(u"addHVChannelButton")

        self.horizontalLayout.addWidget(self.addHVChannelButton)

        self.removeChannelButton = QPushButton(self.gridLayoutWidget_2)
        self.removeChannelButton.setObjectName(u"removeChannelButton")

        self.horizontalLayout.addWidget(self.removeChannelButton)


        self.gridLayout_2.addLayout(self.horizontalLayout, 12, 1, 1, 2)

        self.modelComboBox = QComboBox(self.gridLayoutWidget_2)
        self.modelComboBox.setObjectName(u"modelComboBox")

        self.gridLayout_2.addWidget(self.modelComboBox, 2, 1, 1, 1)

        self.idLineEdit = QLineEdit(self.gridLayoutWidget_2)
        self.idLineEdit.setObjectName(u"idLineEdit")

        self.gridLayout_2.addWidget(self.idLineEdit, 3, 1, 1, 1)

        self.externalServerIPPortLineEdit = QLineEdit(self.gridLayoutWidget_2)
        self.externalServerIPPortLineEdit.setObjectName(u"externalServerIPPortLineEdit")
        sizePolicy3 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.externalServerIPPortLineEdit.sizePolicy().hasHeightForWidth())
        self.externalServerIPPortLineEdit.setSizePolicy(sizePolicy3)
        self.externalServerIPPortLineEdit.setMinimumSize(QSize(200, 0))
        self.externalServerIPPortLineEdit.setMaximumSize(QSize(200, 16777215))

        self.gridLayout_2.addWidget(self.externalServerIPPortLineEdit, 4, 2, 1, 1)

        self.externalServerIPPortLabel = QLabel(self.gridLayoutWidget_2)
        self.externalServerIPPortLabel.setObjectName(u"externalServerIPPortLabel")
        sizePolicy.setHeightForWidth(self.externalServerIPPortLabel.sizePolicy().hasHeightForWidth())
        self.externalServerIPPortLabel.setSizePolicy(sizePolicy)
        self.externalServerIPPortLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.externalServerIPPortLabel, 4, 1, 1, 1)


        self.retranslateUi(PowerSupplyWidget)

        QMetaObject.connectSlotsByName(PowerSupplyWidget)
    # setupUi

    def retranslateUi(self, PowerSupplyWidget):
        PowerSupplyWidget.setWindowTitle(QCoreApplication.translate("PowerSupplyWidget", u"Form", None))
        self.connectionComboBox.setItemText(0, QCoreApplication.translate("PowerSupplyWidget", u"Ethernet", None))
        self.connectionComboBox.setItemText(1, QCoreApplication.translate("PowerSupplyWidget", u"Serial", None))

        self.spacer.setText("")
        self.checkButton.setText(QCoreApplication.translate("PowerSupplyWidget", u"Check", None))
        self.launchServerCheckBox.setText(QCoreApplication.translate("PowerSupplyWidget", u"Launch Server", None))
        self.closeButton.setText(QCoreApplication.translate("PowerSupplyWidget", u"Close", None))
        self.index.setText(QCoreApplication.translate("PowerSupplyWidget", u"Index", None))
        self.idLabel.setText(QCoreApplication.translate("PowerSupplyWidget", u"ID", None))
        self.hvSlotLabel.setText(QCoreApplication.translate("PowerSupplyWidget", u"HV Slot", None))
        self.hvSlotComboBox.setItemText(0, QCoreApplication.translate("PowerSupplyWidget", u"0", None))
        self.hvSlotComboBox.setItemText(1, QCoreApplication.translate("PowerSupplyWidget", u"1", None))
        self.hvSlotComboBox.setItemText(2, QCoreApplication.translate("PowerSupplyWidget", u"2", None))
        self.hvSlotComboBox.setItemText(3, QCoreApplication.translate("PowerSupplyWidget", u"3", None))
        self.hvSlotComboBox.setItemText(4, QCoreApplication.translate("PowerSupplyWidget", u"4", None))
        self.hvSlotComboBox.setItemText(5, QCoreApplication.translate("PowerSupplyWidget", u"5", None))
        self.hvSlotComboBox.setItemText(6, QCoreApplication.translate("PowerSupplyWidget", u"6", None))
        self.hvSlotComboBox.setItemText(7, QCoreApplication.translate("PowerSupplyWidget", u"7", None))
        self.hvSlotComboBox.setItemText(8, QCoreApplication.translate("PowerSupplyWidget", u"8", None))
        self.hvSlotComboBox.setItemText(9, QCoreApplication.translate("PowerSupplyWidget", u"9", None))
        self.hvSlotComboBox.setItemText(10, QCoreApplication.translate("PowerSupplyWidget", u"10", None))
        self.hvSlotComboBox.setItemText(11, QCoreApplication.translate("PowerSupplyWidget", u"11", None))
        self.hvSlotComboBox.setItemText(12, QCoreApplication.translate("PowerSupplyWidget", u"12", None))
        self.hvSlotComboBox.setItemText(13, QCoreApplication.translate("PowerSupplyWidget", u"13", None))
        self.hvSlotComboBox.setItemText(14, QCoreApplication.translate("PowerSupplyWidget", u"14", None))
        self.hvSlotComboBox.setItemText(15, QCoreApplication.translate("PowerSupplyWidget", u"15", None))
        self.hvSlotComboBox.setItemText(16, QCoreApplication.translate("PowerSupplyWidget", u"16", None))

        self.lvSlotLabel.setText(QCoreApplication.translate("PowerSupplyWidget", u"LV Slot", None))
        self.lvSlotComboBox.setItemText(0, QCoreApplication.translate("PowerSupplyWidget", u"0", None))
        self.lvSlotComboBox.setItemText(1, QCoreApplication.translate("PowerSupplyWidget", u"1", None))
        self.lvSlotComboBox.setItemText(2, QCoreApplication.translate("PowerSupplyWidget", u"2", None))
        self.lvSlotComboBox.setItemText(3, QCoreApplication.translate("PowerSupplyWidget", u"3", None))
        self.lvSlotComboBox.setItemText(4, QCoreApplication.translate("PowerSupplyWidget", u"4", None))
        self.lvSlotComboBox.setItemText(5, QCoreApplication.translate("PowerSupplyWidget", u"5", None))
        self.lvSlotComboBox.setItemText(6, QCoreApplication.translate("PowerSupplyWidget", u"6", None))
        self.lvSlotComboBox.setItemText(7, QCoreApplication.translate("PowerSupplyWidget", u"7", None))
        self.lvSlotComboBox.setItemText(8, QCoreApplication.translate("PowerSupplyWidget", u"8", None))
        self.lvSlotComboBox.setItemText(9, QCoreApplication.translate("PowerSupplyWidget", u"9", None))
        self.lvSlotComboBox.setItemText(10, QCoreApplication.translate("PowerSupplyWidget", u"10", None))
        self.lvSlotComboBox.setItemText(11, QCoreApplication.translate("PowerSupplyWidget", u"11", None))
        self.lvSlotComboBox.setItemText(12, QCoreApplication.translate("PowerSupplyWidget", u"12", None))
        self.lvSlotComboBox.setItemText(13, QCoreApplication.translate("PowerSupplyWidget", u"13", None))
        self.lvSlotComboBox.setItemText(14, QCoreApplication.translate("PowerSupplyWidget", u"14", None))
        self.lvSlotComboBox.setItemText(15, QCoreApplication.translate("PowerSupplyWidget", u"15", None))
        self.lvSlotComboBox.setItemText(16, QCoreApplication.translate("PowerSupplyWidget", u"16", None))

        self.saveButton.setText(QCoreApplication.translate("PowerSupplyWidget", u"Save", None))
        self.label_3.setText(QCoreApplication.translate("PowerSupplyWidget", u"Model / Series", None))
        self.indexLabel.setText(QCoreApplication.translate("PowerSupplyWidget", u"IndexLabel", None))
        self.addLVChannelButton.setText(QCoreApplication.translate("PowerSupplyWidget", u"Add LV Channel", None))
        self.addHVChannelButton.setText(QCoreApplication.translate("PowerSupplyWidget", u"Add HV Channel", None))
        self.removeChannelButton.setText(QCoreApplication.translate("PowerSupplyWidget", u"Remove Channel", None))
        self.externalServerIPPortLabel.setText(QCoreApplication.translate("PowerSupplyWidget", u"External server IP:Port", None))
    # retranslateUi

