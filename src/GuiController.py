#------------------------------------------------
#++++++++++GuiController.py##++++++++++++++++++++
#Created by Stefan Maier            07.05.2020
#Last modified by Stefan Maier      19.07.2023
# Email: s.maier@kit.edu
#------------------------------------------------

import yaml
import subprocess, os
import datetime, time, copy
from functools import partial
from tabulate import tabulate

from src.Device import Device
from src.FC7 import FC7
from src.PowerSupply import PowerSupply
from src.Arduino import Arduino
from src.XMLManipulator import XMLManipulator
from src.Measurement import *
from src.Task import *
from src.Run import *
from src.Slot import Slot
from src.Module import Module
from src.Plotter import IvPlot, MonitorPlot, SummaryText

from Dialog.ConfigureSlots import ConfigureSlotsDialog
from Dialog.Monitor import Monitor

from PySide2.QtCore import QObject, Signal, Qt, QDir, QThread
from PySide2.QtWidgets import QInputDialog, QMessageBox, QFileDialog, QDialog, QCheckBox

TEST_DATABSE_NAME           = "trker_int2r" 
PRODUCTION_DATABASE_NAME    = "trker_cmsr"

class GuiController(QObject):

    #Fixed list of possible measurements, Data taking requires Offset and Noise measurement
    measurements = {"IV": False ,"Offset" :False, "Noise": False, "ECV": False, "KIRA": False , "FullTest": False}#,  "Data": False, "FullTest": False}

    controlHubStatus    = Signal(object)          #Tells the GUI to update the controlHubStatus
    #ph2AcfStatus need to be reworked. In best case, delete all devices once it is changed, then it is not necessary anymore
    ph2AcfStatus        = Signal(object, object)  #Tells the GUi to update the ph2AcfStauts TODO check if still necesary/used
    displayConfig       = Signal(object, object)  #Emitted while the config file ist loaded

    #Three main lists to handle devices measurements and the tetrieved results
    deviceList          = []
    threadList          = []
    taskList            = []    #Contains the taskes to be done, e.g. enable power up power supply channels or measurements 
    runList             = []

    measurementSteps    = 0

    #Displays the .root files from the Ph2_ACF
    tbrowser            = None

    #Stores operators and locations
    operatorFilePath    = "settings/operator.yml"
    locationFilePath    = "settings/locations.yml"
    testConditionsFilePath= "settings/testConditions.yml"

    #Contains some informative notes for the user with hints and tips to avoid crash of the program
    notesFilePath       = "settings/notes.yml"

    #Contains the last configuration file of the devices
    devicesFilePath       = "settings/Devices.xml"

    #A slot gives the hardware configuration in which a module can be measured
    slotsFilePath       = "settings/slots.yml"
    slots               = []

    #Stores all the settings done in the GUI and remembers them for the next start-up
    initConfigFilePath  = "settings/guiconfig_init.yml"   #Contains the initial configuration of the gui
    guiConfigFilePath   = "settings/guiconfig.yml"   #Contains the last configuration of the gui
    configurationList   = {"Ph2_ACF_Folder": "", "PowerSupplies_File": ""}  #Dictionary containing the gui configuration

    #DB Interface
    db = None

    CGREEN = '\033[32m'     #For green prints
    CRED   = '\033[91m'     #For red prints
    CEND   = '\033[0m'      #End color prints

    def __init__( self , pWindow ):
        super(GuiController, self).__init__()
        self.window = pWindow
        self.window.windowClosed.connect(self.cleanUp)

        #Monitorobject to display the readout history of the devices 
        self.monitor = Monitor( )
        self.window.ui.monitorLayout.addWidget(self.monitor)
        self.monitor.ui.onlyShowSlotChannelsCheckBox.stateChanged.connect( partial( self.updateConfigList, "Monitor_OnlyShowSlots") )

        #Contains the information how the different power supply types are structures with their channels
        self.powerSupplyModelDictionary = {}
        self.testConditions = {}

        # Link connections from controller to GUI
        #Settings
        self.ph2AcfResponsive = False
        self.controlHubStatus.connect( self.window.updateControlHubStatus )
        self.ph2AcfStatus.connect( self.window.updatePh2AcfStatus )
        self.displayConfig.connect( self.window.displayConfig )
        self.displayConfig.connect( self.monitor.displayConfig )

        # Link connections from GUI to controller
        #Settings  checkControlhub
        self.window.ui.startControlhubButton.clicked.connect( self.startControlhub )
        self.window.ui.checkControlhub.clicked.connect( self.checkIfControlhubIsRunning )
        self.window.ui.checkPh2Acf.clicked.connect( self.checkIfPh2AcfIsResponsive )
        self.window.ui.checkDbButton.clicked.connect( self.initAndCheckDB )
        self.window.ui.updateTestConditionsButton.pressed.connect( partial ( self.getTestConditionsFromDB, True ) )

        self.window.ui.restartDeviceButton.clicked.connect(self.restartDevices)
        self.window.ui.ph2AcfFolderLineEdit.textEdited.connect( partial( self.ph2AcfStatus.emit, False ) )

        #List of paths, can be changed by enetering text or opening a dialog
        self.window.ui.ph2AcfFolderLineEdit.textEdited.connect( partial( self.updateConfigList,"Ph2_ACF_Folder" ) )
        self.window.ui.devicePackageFolderLineEdit.textEdited.connect( partial( self.updateConfigList,"Device_Package_Folder" ) )
        self.window.ui.default2SXmlLineEdit.textEdited.connect( partial( self.updateConfigList,"Default2SHwFile" ) )
        self.window.ui.defaultPSXmlLineEdit.textEdited.connect( partial( self.updateConfigList,"DefaultPSHwFile" ) )
        self.window.ui.resultFolderLineEdit.textEdited.connect(partial( self.updateConfigList, "Result_Folder"))

        self.window.ui.openDialogPh2ACFFolderButton.clicked.connect( partial ( self.newPathSettingsInput, "Ph2_ACF_Folder" ) )
        self.window.ui.openDialogDevicePackageFolderButton.clicked.connect( partial( self.newPathSettingsInput,"Device_Package_Folder" ) )
        self.window.ui.openDialogDefault2SXmlFileButton.clicked.connect( partial ( self.newPathSettingsInput, "Default2SHwFile" ) )
        self.window.ui.openDialogDefaultPSXmlFileButton.clicked.connect( partial ( self.newPathSettingsInput, "DefaultPSHwFile" ) )
        self.window.ui.openDialogResultsFolderButton.clicked.connect( partial ( self.newPathSettingsInput, "Result_Folder" ) )

        self.window.ui.autoCheckStartCheckBox.stateChanged.connect(partial( self.updateConfigList, "AutoCheck_Startup") )
        self.window.ui.powerSupplyDialogCheckBox.stateChanged.connect( partial( self.updateConfigList, "PowerSupply_Dialog") )
        self.window.ui.showPlotCheckBox.stateChanged.connect(partial( self.updateConfigList, "Show_Plot") )
        self.window.ui.invertPlotCheckBox.stateChanged.connect(partial( self.updateConfigList, "Invert_Plot") )

        #Device setting table, add and remove devices and channels
        self.window.ui.addFc7Button.clicked.connect( partial ( self.extendDeviceList , {"Type":"FC7"} ) )
        self.window.ui.addPowerSupplyButton.clicked.connect( partial ( self.extendDeviceList , {"Type":"PowerSupply"} ) )
        self.window.ui.addArduinoButton.clicked.connect( partial ( self.extendDeviceList , {"Type":"Arduino", "Terminator": "LF", "Suffix":"LF"} ) )

        #DB Settings
        self.window.ui.initDbHandlerOnStartupCheckBox.stateChanged.connect( partial( self.updateConfigList, "CheckDB_Startup") )
        self.window.ui.useDbToCheckModulesButton.stateChanged.connect( partial( self.updateConfigList, "CheckModuleWithDB") )
        self.window.ui.TwoFACheckBox.stateChanged.connect( partial( self.updateConfigList, "TwoFA") )
        self.window.ui.productionDBRadioButton.pressed.connect(partial( self.updateConfigList, "DevelopmentDB", '0'))
        self.window.ui.testDBRadioButton.pressed.connect(partial( self.updateConfigList, "DevelopmentDB", '2'))

        self.window.ui.productionDBRadioButton.pressed.connect(partial(self.window.setDbStatus , "Unchecked" ) )
        self.window.ui.testDBRadioButton.pressed.connect(partial(self.window.setDbStatus , "Unchecked" ) )

        #Device tree controls
        self.window.ui.removeDeviceButton.clicked.connect( self.removeDevice )

        #Measurement Tab
        self.window.ui.slotsSpinBox.valueChanged.connect( self.updateSlotInformation )
        self.window.ui.configureSlotsButton.clicked.connect( self.configureSlots )

        self.window.ui.thresholdLineEdit.textChanged.connect(partial( self.updateConfigList,"Threshold" ))
        self.window.ui.latencyLineEdit.textChanged.connect(partial( self.updateConfigList,"Latency" ))
        self.window.ui.extTriggerCheckBox.stateChanged.connect(partial( self.updateConfigList,"External_Trigger" ))

        self.window.ui.testConditionsTableWidget.cellChanged.connect( self.overwriteTestConditions )

        self.window.ui.vtrxLightOffCheckBox.stateChanged.connect(partial( self.updateConfigList,"VTRXLightOff" ))
        self.window.ui.checkFWCheckBox.stateChanged.connect(partial( self.updateConfigList,"CheckFW" ))
        self.window.ui.expertModeCheckBox.stateChanged.connect(partial( self.updateConfigList,"Expert_Mode" ))
        self.window.ui.expertModeCheckBox.stateChanged.connect( self.setExpertMode)


        self.window.ui.operatorComboBox.currentTextChanged.connect( partial( self.updateConfigList, "Operator"))
        self.window.ui.locationComboBox.currentTextChanged.connect( partial( self.updateConfigList, "Location"))
        self.window.ui.stationNameLineEdit.textChanged.connect( partial( self.updateConfigList, "Station_Name"))

        self.window.ui.lpGBTv0RadioButton.pressed.connect(partial( self.updateConfigList, "lpGBT_Version", 0))
        self.window.ui.lpGBTv1RadioButton.pressed.connect(partial( self.updateConfigList, "lpGBT_Version", 1))


        self.window.ui.openDefault2SXmlButton.clicked.connect( partial ( self.openDefaultXml , "2S" ) )
        self.window.ui.openDefaultPSXmlButton.clicked.connect( partial ( self.openDefaultXml , "PS" ) )
        #Measurement start and progress bars
        self.window.ui.startMeasurementButton.clicked.connect( self.checkStartOfMeasurement )
        self.window.ui.stopTaskButton.clicked.connect(self.stopCurrentTask)
        self.window.ui.stopRunButton.clicked.connect(self.stopRun)

        self.window.ui.resultsTreeWidget.itemDoubleClicked.connect(self.resultTreeClicked)

        self.window.ui.addOperatorButton.clicked.connect(self.addOperator)


        #Add a checkbox for each possible measurement
        for measurement, value in self.measurements.items():
            measurementCheckBox = QCheckBox(measurement)
            self.window.ui.measurementCheckBoxes.addWidget(measurementCheckBox)
            measurementCheckBox.stateChanged.connect(partial ( self.newMeasurementInput,  measurement ) )
            if measurement in ["Data", "KIRA"]:#Not implemented so far TODO implement KIRA and condition data measurements
                measurementCheckBox.setDisabled(True)

        #Link the device signals to the corresponding methods
        self.window.deviceCommand.connect( self.executeDeviceCommand )
        self.window.taskStopSignal.connect( self.stopTask )

        #Buttons to handle the measurement results
        self.window.ui.convertResultsButton.clicked.connect( self.createCSVandZIPFiles )
        self.window.ui.uploadResultsButton.clicked.connect( self.uploadCSVFilesToDb )
        self.window.ui.convertAllResultsButton.clicked.connect( self.convertAllResultFilesToXml )
        self.window.ui.uploadAllResultsButton.clicked.connect( self.uploadAllXmlFilesToDb )
        self.window.ui.loadResultFolderButton.clicked.connect( self.loadResultFolder )

        #Test button
        #self.window.ui.testButton.hide()
        self.window.ui.testButton.clicked.connect ( self.test )


    #Called once to actually start the GUI with the initialisation via the config files

    def start ( self ):
        self.window.showModuleTestSettings(False)
        #This part will be implemented later in an expert usage, therefore hide it

        #Create needed folder structure if someone accidentally deletes it
        if not os.path.exists('./settings/Devices'):
            os.makedirs('./settings/Devices')
        if not os.path.exists('./settings/Measurements'):
            os.makedirs('./settings/Measurements')
        if not os.path.exists('./settings/Tasks'):
            os.makedirs('./settings/Tasks')
        if not os.path.exists('./logs'):
            os.makedirs('./logs')

        if not os.path.isfile(self.devicesFilePath):
            file = open(self.devicesFilePath, 'w')
            file.close()

        #Slot = Module test bench, Operators is a simple list as well as notes
        self.loadOperators()
        self.loadNotes()
        self.loadLocations()
        self.loadTestConditions()


        #Load the configuration file to restore recent settings
        self.loadInitConfigFile()

        #Disable critical settings
        self.setExpertMode( int ( self.configurationList.get("Expert_Mode",0) ) > 0 )

        self.setEnvironmentVariablesForPh2ACF()

        #Check wheter the controlhub is running and the Ph2_ACF is reachable
        if self.checkIfControlhubIsRunning():
            self.checkIfPh2AcfIsResponsive()
        else:
            self.startControlhub()
            self.checkIfPh2AcfIsResponsive()

        #Load the devices from the device file. Needs to be done after the tcp server starts so the clients cann connect
        self.loadPowerSupplyChannelNamingFile("settings/PowerSupplyModels.yml")

        self.restartDevices(pCheck = False)           #restart Devices instead of load them from Config


        #If wished, check connection to devices and DB during startup
        if int( self.configurationList.get("AutoCheck_Startup",0) ) > 0:
            for device in self.deviceList:
                self.executeDeviceCommand( device.index, "Check", None, None)

        if int( self.configurationList.get("CheckDB_Startup",0) ) > 0:
            self.initAndCheckDB()
        else:
            self.window.setDbStatus("Unchecked")

    def setExpertMode( self, pMode):
        if pMode:
            print("GIPHT:\tEXPERT MODE ENABLED")
            self.window.ui.testButton.show()
        else:
            print("GIPHT:\tEXPERT MODE DISABLED")
            self.window.ui.testButton.hide()

        self.window.ui.vtrxLightOffCheckBox.setDisabled(not pMode)
        self.window.ui.devicePackageFolderLineEdit.setDisabled(not pMode)
        self.window.ui.ph2AcfFolderLineEdit.setDisabled(not pMode)
        self.window.ui.testConditionsTableWidget.setDisabled(not pMode)


    def udpdateDbStatus(self, pStatus):
        self.window.setDbStatus(pStatus)
        print("GIPHT:\tDatabase status: "+ pStatus)

    def initAndCheckDB(self):
        self.udpdateDbStatus("Unchecked")

        self.db = DB_Interface(int(self.configurationList["DevelopmentDB"]) > 0, int(self.configurationList["TwoFA"]) > 0)
        checkDbTask = CheckDB(len(self.taskList), self.db)
        checkDbTask.dbStatus.connect(self.udpdateDbStatus)
        checkDbTask.taskDone.connect(self.loadLocations)
        checkDbTask.taskDone.connect(self.getTestConditionsFromDB)

        self.extendTaskList( checkDbTask )
        #self.doNextTask()
        self.executeTask( checkDbTask )

    def getTestConditionsFromDB( self, pCaller ):
        if self.db is not None:
            testConditions = GetTestConditionsFromDB(len(self.taskList), self.db)
            testConditions.dbStatus.connect(self.udpdateDbStatus)
            testConditions.taskDone.connect(self.loadTestConditions)

            self.extendTaskList( testConditions )
            #If this method is called manually, start the task. otherwise it is called via the check tb task
            if isinstance(pCaller,bool) and pCaller:
                self.executeTask( testConditions )


    #-----------------------------------------------------------------------
    #------ Load init file from disk to restore previous cofigurations -----
    #-----------------------------------------------------------------------

    def loadInitConfigFile ( self ):
        print("GIPHT:\tLoad last configuration from file " + self.guiConfigFilePath)

        if not os.path.isfile(self.guiConfigFilePath):
            os.system("cp " + self.initConfigFilePath + " " + self.guiConfigFilePath)

        with open(self.guiConfigFilePath, 'r') as f:
            self.configurationList = yaml.safe_load(f)

        for key, value in self.configurationList.items():
            print("GIPHT:\tLoad:\t" + key + ":\t" + str(value))
            self.displayConfig.emit(key, value)

    def saveConfigFile( self ):
        print("GIPHT:\tUpdate configuration file " + self.guiConfigFilePath)
        with open(self.guiConfigFilePath, "w") as f:
            yaml.dump(self.configurationList,f)

    #Operators can be added via a popup, for removal the operator file must be edited
    def addOperator( self ):
        name, ok = QInputDialog.getText(None, 'New Operator', 'Enter Name')
        if ok:
            self.window.ui.operatorComboBox.addItem(name)
            names = [self.window.ui.operatorComboBox.itemText(i) for i in range(self.window.ui.operatorComboBox.count())]

            print("GIPHT:\tAdd " + name + " to operator list")
            with open(self.operatorFilePath, 'w') as f:
                yaml.dump(names,f)

    #Read the operator file from disk and fil the combobox
    def loadOperators( self ):
        print("GIPHT:\tLoad operators from file " + self.operatorFilePath)

        #Open or create file
        if not os.path.isfile(self.operatorFilePath):
            open (self.operatorFilePath, 'w+')

        with open(self.operatorFilePath, 'r') as f:
            operators = []
            operators = yaml.safe_load(f)
            if operators is not None:
                if len(operators):
                    self.window.ui.operatorComboBox.blockSignals(True)
                    self.window.ui.operatorComboBox.addItems(operators)
                    self.window.ui.operatorComboBox.blockSignals(False)
                else:
                    print("GIPHT:\tOperator list empty")
            else:
                print("GIPHT:\tOperator list empty")

        for index in range(self.window.ui.operatorComboBox.count()): 
            print("GIPHT:\tOperator:\t" + self.window.ui.operatorComboBox.itemText(index))

    def loadLocations( self ):
        locations = []
        if self.db is None:
            print("GIPHT:\tLoad locations from file " + self.locationFilePath)
            #Open or create file
            if not os.path.isfile(self.locationFilePath):
                open (self.locationFilePath, 'w+')

            with open(self.locationFilePath, 'r') as f:
                locations = yaml.safe_load(f)

            for index in range(self.window.ui.locationComboBox.count()): 
                print("GIPHT:\tLocation:\t" + self.window.ui.locationComboBox.itemText(index))
        else:
            print("GIPHT:\tGot locations from DB")
            locations = self.db.locations
            if not os.path.isfile(self.locationFilePath):
                open (self.locationFilePath, 'w+')

            with open(self.locationFilePath, 'w') as f:
                yaml.dump(locations,f)

        if not locations == [] and locations is not None:
            if len(locations):
                self.window.ui.locationComboBox.blockSignals(True)
                self.window.ui.locationComboBox.addItems([location.get("locationName","") for location in locations])
                self.window.ui.locationComboBox.blockSignals(False)
            else:
                print("GIPHT:\tLocation list empty")
        else:
            print("GIPHT:\tLocation list empty")

    def loadTestConditions( self ):
        if self.db is None:
            print("GIPHT:\tLoad test conditions from file " + self.testConditionsFilePath)
            #Open or create file
            if not os.path.isfile(self.testConditionsFilePath):
                open (self.testConditionsFilePath, 'w+')

            with open(self.testConditionsFilePath, 'r') as f:
                self.testConditions = yaml.safe_load(f)

            #for index in range(self.window.ui.locationComboBox.count()): 
            #    print("GIPHT:\tLocation:\t" + self.window.ui.locationComboBox.itemText(index))
        else:
            print("GIPHT:\tGet test conditions from DB Interface")
            self.testConditions = self.db.testConditions
            if not os.path.isfile(self.testConditionsFilePath):
                open (self.testConditionsFilePath, 'w+')

            with open(self.testConditionsFilePath, 'w') as f:
                yaml.dump(self.testConditions,f)

        if self.testConditions is not None:
            self.window.ui.testConditionsTableWidget.blockSignals(True)
            self.window.updateTestConditions(self.testConditions)
            self.window.ui.testConditionsTableWidget.blockSignals(False)
        else:
            print("GIPHT:\tTest conditions list empty")

    def overwriteTestConditions ( self, pRow, pColumn):#pTestConditions ):
        #Give out warning
        print("GIPHT:\tWARNING! Test condition cell changed ", pRow, pColumn)

        ok = QMessageBox.question(None, "WARNING", "Do you want to\noverwrite the test conditions?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if ok == QMessageBox.Yes:
            self.testConditions = self.window.getTestConditions()

            if not os.path.isfile(self.testConditionsFilePath):
                open (self.testConditionsFilePath, 'w+')

            with open(self.testConditionsFilePath, 'w') as f:
                yaml.dump(self.testConditions,f)
        else:
            self.loadTestConditions()

    #Notes are some tips, hints to point to bugs or how to use the gui in general
    def loadNotes( self ):
        print("GIPHT:\tLoad Notes from file " + self.notesFilePath)
        notes = []
        with open(self.notesFilePath, 'r') as f:
            try:
                notes = yaml.safe_load(f)
            except:
                print("GIPHT:\tNote list empty")

        for note in notes:
            if note is not None:
                print("\t" + note)
            else:
                print(" ")
        self.window.showNotes( notes )

    #The configurationList contains all the settings loaded from the .yml file
    def updateConfigList ( self, pConfig, pValue, pReprint = False ):
        print("GIPHT:\tChange configuration: " + pConfig + ":\t" + str(pValue))
        self.configurationList[pConfig] = str(pValue)
        if pReprint:
            self.window.displayConfig( pConfig, str( pValue ) )
            self.monitor.displayConfig( pConfig, str( pValue ) )

        self.saveConfigFile()


    #Generates a dialog to choose file settings, opens a popup to search for dedicated settings files
    def newPathSettingsInput( self, pConfig ):
        forOpen = True
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog

        #options |= QFileDialog.DontUseCustomDirectoryIcons
        dialog = QFileDialog()
        dialog.setWindowTitle("Choose " + pConfig)
        dialog.setOptions(options)
        dialog.setDirectory(os.getcwd())
        dialog.setFilter(dialog.filter() | QDir.Hidden)
        
        if pConfig in ["Ph2_ACF_Folder", "Result_Folder","Device_Package_Folder"]:
            dialog.setFileMode(QFileDialog.DirectoryOnly)
        elif pConfig in ["Default2SHwFile", "DefaultPSHwFile"]:
            dialog.setDirectory(self.configurationList["Ph2_ACF_Folder"])
        # OPENING OR SAVING
        dialog.setAcceptMode(QFileDialog.AcceptOpen) if forOpen else dialog.setAcceptMode(QFileDialog.AcceptSave)

        if dialog.exec_() == QDialog.Accepted:
            path = dialog.selectedFiles()[0]  # returns a list
            if pConfig in ["Ph2_ACF_Folder", "Result_Folder","Device_Package_Folder"]:
                self.updateConfigList( pConfig, path + "/", True)
            elif pConfig in ["Default2SHwFile", "DefaultPSHwFile"]:
                self.updateConfigList( pConfig, path, True)

    #Load the slot configuration from the slot configuration file
    def loadSlots ( self ):
        print("GIPHT:\tLoad slot configuration from file " + self.slotsFilePath)
        slotsDict = []
        self.slots.clear()
        self.window.clearSlots()

        #Open or create file
        if not os.path.isfile(self.slotsFilePath):
            open (self.slotsFilePath, 'w+')
        with open(self.slotsFilePath, "r") as f:
            try:
                slotsDict = yaml.safe_load(f)
            except JSONDecodeError:
                print("GIPHT:\tSlot file empty, slots not loaded")

        if slotsDict is not None:
            print("GIPHT:\tSlot:\tLV\tHV\tArduino")
            for slotDict in slotsDict:
                devices = {}
                devices["LV_PowerSupply"],devices["LV_Channel"] = self.getPowerSupplyAndChannel( slotDict["LV_PowerSupply"], slotDict["LV_Channel"])
                devices["HV_PowerSupply"],devices["HV_Channel"] = self.getPowerSupplyAndChannel( slotDict["HV_PowerSupply"], slotDict["HV_Channel"])
                devices["Arduino"]                              = self.getArduino( slotDict["Arduino"])
 
                newSlot = Slot( len ( self.slots ), devices,  )
                newSlot.newModule.connect(self.bookModule)
                newSlot.statusWidget.ui.slotCheckBox.stateChanged.connect(partial(self.checkActiveSlots, newSlot.index))

                self.slots.append(newSlot)
                self.window.addSlotStatus(newSlot.statusWidget)

        self.window.setSlotNumber(len ( self.slots )  )
    
    #Save the slot configuration to the slot configuration file
    def saveSlots( self ):
        print("GIPHT:\tSave slot configuration to file " + self.slotsFilePath)
        slotsDict = []
        for slot in self.slots:
            slotsDict.append(slot.getSlotDictionary())
            #print(slot.getSlotDictionary())
        
        with open(self.slotsFilePath, "w") as f:
            yaml.dump(slotsDict,f)
    
    def checkActiveSlots(self, pSlotIndex, pActivate):
        print("Slotnumber:" + str(pSlotIndex))
        print("State:" + str(pActivate))

        if pActivate:
            for slot in self.slots:
                slot.statusWidget.ui.slotCheckBox.setChecked(False)
                #print(slot.active)

            [slot for slot in self.slots if slot.index == pSlotIndex][0].statusWidget.ui.slotCheckBox.setChecked(True)

    #Add or remove a slot
    def updateSlotInformation( self , pNumberOfSlots ):
        #If new number is smaller, remove a slot
        if pNumberOfSlots < len( self.slots ):
            del self.slots[-1]
            self.window.removeSlotStatus()
        #If larger add a book module widget
        elif pNumberOfSlots > len( self.slots ):
            newSlot = Slot( len ( self.slots ) )
            newSlot.newModule.connect(self.bookModule)
            self.slots.append(newSlot)
            self.window.addSlotStatus(newSlot.statusWidget)

        for slot in self.slots:
            slot.updateDeviceSignals()
    

        self.saveSlots()

    #Returns the actual power supply and arduino object based on their IDs
    def getPowerSupplyAndChannel( self, pPowerSupplyId, pChannelId ):
        searchedPowerSupply     = None
        searchedChannel         = None
        for device in self.deviceList:
            if device.config["ID"] == pPowerSupplyId:
                searchedPowerSupply = device
                for channel in device.channels:
                    if channel.config["ID"] == pChannelId:
                        searchedChannel = channel
                        break
                break
        return searchedPowerSupply, searchedChannel

    def getArduino( self, pArduinoId ):
        searchedArduino = None
        for device in self.deviceList:
            if device.config["ID"] == pArduinoId:
                searchedArduino = device
                break
        return searchedArduino


    #-----------------------------------------------------------------------
    #-------- Check and configuration Ph2_ACF and  Controlhub --------------
    #-----------------------------------------------------------------------

    #The controlhubg should be running on the local machine, searches if a matching process is running
    #Must be checked on other systems as well

    def checkIfControlhubIsRunning ( self ):
        running = False
        process_name= "controlhub"
        ps     = subprocess.Popen("ps -eafww | grep "+process_name, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE )
        output = ps.stdout.read()
        ps.stdout.close()
        ps.wait()

        running = "-progname controlhub" in str(output)
        self.controlHubStatus.emit(running)
        print("GIPHT:\tControlhub: " + str(running))
        return running

    #Execute Ph2 ACF systemtest command to see whether it is responsive
    #Requires a gipht-supporting Ph2_ACF!
    def checkIfPh2AcfIsResponsive( self ):
        try:
            process = subprocess.Popen("source setup.sh && gipht_systemtest", stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding = 'UTF-8', shell= True, executable="/bin/bash", cwd=self.configurationList["Ph2_ACF_Folder"])
            outs, errs = process.communicate(input = None, timeout = 5)
        except:
            print(errs)
            print(outs)
            outs = "Ph2_ACF not responsive"

        responsive = "D19cFWInterface Constructor" in outs
        self.ph2AcfStatus.emit(responsive, self.configurationList["Ph2_ACF_Folder"])
        print("GIPHT:\tPh2_ACF: " + str(responsive))
        self.ph2AcfResponsive = responsive
        return responsive


    #-----------------------------------------------------------------------
    #----------------------------  Devices  --------------------------------
    #-----------------------------------------------------------------------

    #Device object factory
    def getNewDevice(self, pDeviceIndex, pConfig):
        if pConfig is not None:
            if pConfig["Type"] == "PowerSupply":
                newDevice = PowerSupply(pDeviceIndex,
                                        pConfig,
                                        self.powerSupplyModelDictionary,
                                        self.configurationList["Device_Package_Folder"])
            elif pConfig["Type"] == "Arduino":
                newDevice = Arduino(pDeviceIndex,
                                    pConfig,
                                    self.configurationList["Device_Package_Folder"])
            elif pConfig["Type"] == "FC7":
                newDevice = FC7(pDeviceIndex,
                                pConfig,
                                self.configurationList["Ph2_ACF_Folder"],
                                self.configurationList["FC7_Address_Table"])
            else:
                newDevice = Device (pDeviceIndex, {}, None )
        else:
            newDevice = Device (pDeviceIndex, {}, None )
        return newDevice

    #Depending on which butten is pressed, add a new device to the list. Only 1 FC7 allowed!
    def extendDeviceList( self, pDeviceConfigSkeleton ):
        pDeviceConfig = copy.deepcopy(pDeviceConfigSkeleton)
        if pDeviceConfig.get("Type","") == "FC7":
            if "FC7" in [device.config["Type"] for device in self.deviceList]:
                print("GIPHT:\tOnly one FC7 allowed!")
                return
        newIndex = len(self.deviceList)
        newDevice = self.getNewDevice( newIndex, pDeviceConfig )
        newDevice.deviceCommand.connect( self.executeDeviceCommand )

        self.deviceList.append ( newDevice )
        self.window.addDevice  ( newDevice )
        self.monitor.addDeviceToMonitor( newDevice )

        self.checkDeviceRestart()

    #For the sake of comlexity, only the last item can be removed, sorry
    def removeDevice( self ):
        if len(self.deviceList) > 0:
            del self.deviceList[-1]
            self.window.removeDevice()
            self.window.hideDeviceConfigs()
        self.saveDevices()

    #searches for processes with "PowerSupplyCont" and kills them
    def killServers( self ):
        #If there is a server launched outisde of gipht, try to only kill servers linked to the devices
        if False in [device.hwInterface.launch for device in self.deviceList if device.config["Type"] != "FC7"]:
            print("GIPHT:\tAt least one device uses an external TCP server. Try to only kill servers linked to the devices. \
                         \t\tThis might miss old servers from previous instances of gipht!")
            for device in self.deviceList:
                if device.config["Type"] != "FC7" and device.hwInterface.server is not None:
                    if device.hwInterface.server.pid is not None:
                        try:
                            os.kill(device.hwInterface.server.pid, 9)
                        except ProcessLookupError:
                            print("GIPHT:\tDevice " + device.config["ID"] + " - no process to kill")
        else:
            print("GIPHT:\tKill all local servers launched by POWDER")
            search = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
            output, error = search.communicate()
            target_process = "PowerSupplyCont"

            for line in output.splitlines():
                if target_process in str(line):
                    pid = int(line.split(None, 1)[0])
                    os.kill(pid, 9)
        
    def restartDevices( self, pDummy=True, pCheck = True ): #pDummy is necessary to catch and ignore the parameter givne by button clicked command
        self.window.setGiphtBusy(True)
        self.stopDevices()
        self.startDevices(pCheck)
        self.window.setGiphtBusy(False)

    def stopDevices( self ):
        self.window.hideDeviceConfigs()
        #First, kill all possible TCP servers of the devices
        self.killServers()
        self.monitor.clear()

    #Is done once the Device.xml is changed, then the TCP server is restarted with the new file
    def startDevices( self, pCheck ):
        self.loadDevicesFromConfig()
        if pCheck:
            for device in self.deviceList:
                self.executeDeviceCommand( device.index, "Check", None, None)

        self.loadSlots()
        self.window.ui.restartDeviceButton.setStyleSheet("background-color: green")

    #Depending on the input, execute the commands on the different devices. Done during the device configuration input from user, not measurements
    def executeDeviceCommand( self, pDeviceIndex, pCommand, pValue, pChannelIndex):
        #print("Index: " + str(pDeviceIndex) + "\tCommand: " + str(pCommand) + "\tValue: " + str(pValue))

        newTasks = []
        if pCommand == "Save":
            self.saveDevices(pValue)
        elif pCommand == "Check":
            newTasks.append( Check(len(self.taskList), self.deviceList[pDeviceIndex]) )
        elif pCommand == "UploadArduinoSoftware":
            self.compileAndUploadArduinoSoftware(pValue)
        elif pCommand == "KIRASettings":
            newTasks.append( KIRASettings( len(self.taskList), self.deviceList[pDeviceIndex], pValue) )
        elif pCommand == "SwitchFW":
            newTasks.append( SwitchFirmware( len(self.taskList) , self.deviceList[pDeviceIndex] , pValue) ) #pValue = FirmwareName
        elif pCommand == "UploadFW":
            newTasks.append( UploadFirmware( len(self.taskList) , self.deviceList[pDeviceIndex] , pValue) ) #pValue = Firmware (Bitstream)
            newTasks.append( Check(len(self.taskList) + 1 , self.deviceList[pDeviceIndex], True) )
            newTasks.append( SwitchFirmware( len(self.taskList) + 2  , self.deviceList[pDeviceIndex] , pValue) ) 
        elif pCommand == "EnableDisable":
            newTasks.append( EnableDisableChannel( len(self.taskList), self.deviceList[pDeviceIndex] , pChannelIndex ) )
        elif pCommand == "v_set":
            newTasks.append( SetVoltages( len(self.taskList) , [ [self.deviceList[pDeviceIndex] , self.deviceList[pDeviceIndex].channels[pChannelIndex] , pValue ] ] ) ) #pValue = Voltage
        elif pCommand == "i_max":
            print("IMAX TO BE IMPLEMENTED (Modify power_supply package)")
        elif pCommand == "Start MonitorPoll":
            newTasks.append( MonitorPoll( len(self.taskList), self.deviceList[pDeviceIndex]) )
        elif pCommand == "Start MonitorPoll with Saving":
            newTasks.append( MonitorPoll( len(self.taskList), self.deviceList[pDeviceIndex], pValue) )
        elif pCommand == "Stop MonitorPoll":
            #Stopping the monitor works by setting the run flag to false
            [self.stopTask(task) for task in self.taskList if (task.type == "MonitorPoll"  and task.device == self.deviceList[pDeviceIndex]) ]

        if len(newTasks):
            self.extendTaskList( newTasks )
            self.executeTask(newTasks[0])

    def extendTaskList( self, pNewTasks ):
        if isinstance(pNewTasks, list):
            for task in pNewTasks:
                task.statusUpdate.connect(self.window.updateTask)
                self.taskList.append(task)            
                self.window.addTask(task)
        else:
            pNewTasks.statusUpdate.connect(self.window.updateTask)
            self.taskList.append(pNewTasks)            
            self.window.addTask(pNewTasks)
    #-----------------------------------------------------------------------
    #------------------ Load Device and Models files------------------------
    #-----------------------------------------------------------------------

    #If already done from another file, clear the tree and reload the list with the new xml file
    def loadDevicesFromConfig( self ):
        self.window.clearDeviceTreeWidget()
        self.deviceList.clear()
        xmlLoader = XMLManipulator()
        deviceConfigList = xmlLoader.getDevicesFromFile(self.devicesFilePath)
        for deviceConfig in deviceConfigList:
            self.extendDeviceList(deviceConfig)

    #Load the hardcoded .yml file containing the different models, how many channels they have , how they are named and if it is LV or HV
    def loadPowerSupplyChannelNamingFile( self , pFile):
        print("GIPHT:\tLoad power supply models file " + pFile)
        with open(pFile, 'r') as f:
            self.powerSupplyModelDictionary = yaml.safe_load(f)

        print("GIPHT:\tModels loaded:")
        for key, value in self.powerSupplyModelDictionary.items():
            print("\t" + key + ":\t" + str(value))

    #Saves the information of the current power supplies to an XML which is reloaeded by program startup
    def saveDevices ( self, pRestart = True ):
        xmlLoader = XMLManipulator()
        xmlLoader.saveDevicesToFile(self.devicesFilePath, self.deviceList)
        #self.window.ui.restartDeviceButton.setStyleSheet("background-color: orange")
        if pRestart:
            self.checkDeviceRestart()

    def checkDeviceRestart ( self ):
        self.window.ui.restartDeviceButton.setStyleSheet("background-color: orange")
        psList = [device for device in self.deviceList if device.config["Type"] == "PowerSupply"]
        valid = True
        name = ""
        for ps in psList:
            if len(ps.channels) == 0:
                valid = False
                name = ps.config["ID"]

        if valid:
            self.window.ui.restartDeviceButton.setEnabled(True)
        else:
            print("GIPHT:\tCannot read our power supply (" + name + ") with no channels! Please add!")
            self.window.ui.restartDeviceButton.setEnabled(False)

    #-----------------------------------------------------------------------
    #------------------------- Handle Measurements -------------------------
    #-----------------------------------------------------------------------

    #Handles the input of the measurement checkboxes. Updats the corresponding measurement object linked to the board
    def newMeasurementInput( self, pMeasurement, pState ):
        self.measurements[pMeasurement] = bool(pState)
        #Catch some excepctions e.g. no Noise without Offset
        if not self.measurements["Offset"] and pMeasurement == "Offset":
            self.measurements["Noise"] = False
            self.measurements["KIRA"] = False

        if self.measurements["Noise"] and pMeasurement == "Noise":
            self.measurements["Offset"] = True

        if self.measurements["KIRA"] and pMeasurement == "KIRA":
            self.measurements["Offset"] = True

        #Fulltest activates all measurements
        if pMeasurement == "FullTest" and bool(pState):
            for key, item in self.measurements.items():
                if key not in ["KIRA", "Data", "ECV"]:
                    self.measurements[key] = True
        else:
            if not bool(pState):
                self.measurements["FullTest"] = False

        self.window.updateMeasurementInfo( self.measurements )

    #Called once the Book Modules button is pressed. Opens the Dialog with and gives the already booked modules of the current board to the dialog
    def configureSlots( self ):
        self.configureSlotsDialog = ConfigureSlotsDialog( self.slots, self.deviceList )
        self.configureSlotsDialog.show()
        self.configureSlotsDialog.sendSlots.connect(self.handleSlotChanges)

    #Called when the slot configuration is changed or when a module id is entered, Checks if the module is in the database by trying to retrieve some information
    def bookModule( self, pSlotNumber ):
        moduleId = self.slots[pSlotNumber].getModuleIdText()

        if moduleId == "":
            self.slots[pSlotNumber].setModuleTextColor("white")
        else:
            self.slots[pSlotNumber].setModuleTextColor("green")

            #Will be changed, either information about module musst be given manually, or it comes from the DB
            #TODO rework module constructor, information input
            newModule = Module ( moduleId )

            print("GIPHT:\tAdd module " + moduleId + " to Slot " + str(pSlotNumber))
            self.slots[pSlotNumber].module = newModule

            self.slots[pSlotNumber].statusWidget.ui.encapsulatedCheckBox.setChecked(False)
            self.slots[pSlotNumber].statusWidget.ui.encapsulatedCheckBox.setEnabled(True)

            self.slots[pSlotNumber].statusWidget.ui.skeletonCheckBox.setChecked(False)
            self.slots[pSlotNumber].statusWidget.ui.skeletonCheckBox.setEnabled(True)

            if (  int ( self.configurationList["CheckModuleWithDB"] ) > 0):
                moduleInformation = {}
                try: 
                    moduleInformation = self.db.getModuleInformation(moduleId)[0]
                except:
                    print("GIPHT:\tModule Information could not be retrieved from CMS construction DB")
                self.slots[pSlotNumber].statusWidget.ui.moduleTypeComboBox.setDisabled(True)
                if "2S" in moduleId:
                    self.slots[pSlotNumber].statusWidget.ui.moduleTypeComboBox.setCurrentText("2S")
                elif "PS" in moduleId and "_05_" in moduleId:
                    self.slots[pSlotNumber].statusWidget.ui.moduleTypeComboBox.setCurrentText("PS_5G")
                elif "PS" in moduleId and "_5_" in moduleId:
                    self.slots[pSlotNumber].statusWidget.ui.moduleTypeComboBox.setCurrentText("PS_5G")
                elif "PS" in moduleId and "_10_" in moduleId:
                    self.slots[pSlotNumber].statusWidget.ui.moduleTypeComboBox.setCurrentText("PS_10G")
                elif "PS" in moduleId and moduleInformation.get("lpgbtBandwidth","") == "5Gbps":
                    self.slots[pSlotNumber].statusWidget.ui.moduleTypeComboBox.setCurrentText("PS_5G")
                elif "PS" in moduleId and moduleInformation.get("lpgbtBandwidth","") == "10Gbps":
                    self.slots[pSlotNumber].statusWidget.ui.moduleTypeComboBox.setCurrentText("PS_10G")
                else:
                    print("GIPHT:\tModule type could not be identified. Please choose manually")
                    self.slots[pSlotNumber].statusWidget.ui.moduleTypeComboBox.setDisabled(False)

                #print(moduleInformation)
                if moduleInformation:
                    newModule.information = moduleInformation
                    self.slots[pSlotNumber].setModuleTextColor("green")
                    try:
                        self.slots[pSlotNumber].statusWidget.ui.infoButton.clicked.disconnect()
                    except:
                        pass
                    self.slots[pSlotNumber].statusWidget.ui.infoButton.setDisabled(False)
                    self.slots[pSlotNumber].statusWidget.ui.infoButton.clicked.connect( partial ( self.displayModuleInfo, newModule ) ) 
                else:
                    print("GIPHT:\tModule not in DB!")
                    self.slots[pSlotNumber].setModuleTextColor("red")
                    self.slots[pSlotNumber].statusWidget.ui.infoButton.setDisabled(True)
            else:
                self.slots[pSlotNumber].setModuleTextColor("green")
                self.slots[pSlotNumber].statusWidget.ui.infoButton.setDisabled(True)

    #Generates a small messagebox listing all relevant information about the module. Information is retrieved from DB
    def displayModuleInfo ( self, pModule ):
        msgBox = QMessageBox()
        msgBox.setIcon(QMessageBox.Information)
        display = ""
        header = ["Attribute","Value"]
        table = []
        for key, value in pModule.information.items():
            display += str(key) + "\t" + str(value) + "\n"
            table.append([str(key),str(value)])
        
        msgBox.setText(display)#tabulate(table,headers=header, tablefmt='pipe'))
        msgBox.setWindowTitle("Information " + str ( pModule.id ) )
        msgBox.setStandardButtons(QMessageBox.Ok) # | QMessageBox.Cancel
        returnValue = msgBox.exec()


    #Handles the information of the new modules given by the Slots
    def handleSlotChanges ( self, pSlots ):
        #Reset all modules booked, and reset the link to the power supplies
        self.slots = pSlots
        self.saveSlots()
        module_cnt =  0
        for slot in self.slots:
            if slot.module is not None:
                module_cnt += 1

    #Opens default measurement XML with standard file editor
    def openDefaultXml( self, p2SorPS ):
        if p2SorPS == "2S":
            subprocess.Popen( [ "gio", "open" , self.configurationList["Default2SHwFile"] ] )
        elif p2SorPS == "PS":
            subprocess.Popen( [ "gio", "open" , self.configurationList["DefaultPSHwFile"] ] )

    #Rather complex method to define which measurements are done with which configuration, IV data can be taken with condition data etc.
    def checkStartOfMeasurement( self ):
        #Before telling the board to start the measurement, first check if
        #   - FC7 is available
        #   - Measurements are selected
        #If there are one or more reasons to not do the measurement it is aborted

        #Check the measurements that are booked for the board
        #The measurements have to be put in a correct order (as in the measurement List defined)

        print("GIPHT:\tCheck start of measurement")
        selectedMeasurements = [measurement for measurement,value in self.measurements.items() if value]

        reasons = []

        #if len ( [device for device in self.deviceList if device.config["Type"] == "FC7"] ) == 0:
        #    reasons.append("\t\tNo FC7 in the device list")

        if len( [value for measurement, value in self.measurements.items() if value ]  ) == 0:
            reasons.append("\t\tNo measurement(s) selected")


        #Get the first active slot and the linked test conditions
        slot = [slot for slot in self.slots if slot.active][0]
        testConditions = self.testConditions["2S"]
        if slot is not None:
            if slot.module.skeleton:
                testConditions = self.testConditions["Skeleton"]
            else:
                testConditions = self.testConditions[slot.getModuleType()]
            if slot.lvPowerSupplyChannel is None:
                reasons.append("Slot " + str(slot.index) + " does not have a LV Channel")
            if slot.hvPowerSupplyChannel is None:
                reasons.append("Slot " + str(slot.index) + " does not have a HV Channel")
            if slot.arduino is None and "KIRA" in selectedMeasurements:
                reasons.append("Slot " + str(slot.index) + " does not have an arduino, KIRA test not possible")
            if slot.module.id is None:
                reasons.append("\t\tModule not booked")
        else:
            reasons.append("Slot not activated")      

        #No reasons to abort found, lets add the measurment objects and start it
        if len(reasons) == 0:
            timestampfile = os.getcwd() + "/logs/timestamps.yml"
            if os.path.isfile(timestampfile):
                os.remove(timestampfile)

            #Update the local measurement counter
            self.updateConfigList("LocalRunNumber" , str( int( self.configurationList["LocalRunNumber"] ) + 1 ) )
            #Everything is fine, start measurement
            runInfo = {}
            runInfo["Operator"]        = self.window.ui.operatorComboBox.currentText()
            runInfo["Location"]        = self.window.ui.locationComboBox.currentText()
            runInfo["Station_Name"]    = self.window.ui.stationNameLineEdit.text()

            runInfo["Result_Folder"]   = self.configurationList["Result_Folder"] + str ( int( self.configurationList["LocalRunNumber"] ) ) + "/"
            runInfo["LocalRunNumber"]  = int( self.configurationList["LocalRunNumber"] )
            runInfo["Date"]            = str( datetime.now().strftime("%d-%m-%Y %H:%M:%S") )

            runInfo["Run_type"]    = "mod_final" if slot.module.encapsulated else "mod_bonded"
            runInfo["Module_ID"]    = slot.module.id

            self.runList.append(Run(runInfo))

            print("GIPHT:\t" + self.CGREEN + "Configured measurements " + self.CEND)
            [print(self.CGREEN + "\t\t" + measurement + self.CEND) for measurement,value in self.measurements.items() if value ]

            self.measurementSteps = len( self.taskList )
            #Some measurements are needed for each individual module, others for all modules at once

            if slot.arduino is not None:
                kiraOff = KIRAOff(len(self.taskList), slot.arduino, True)
                self.extendTaskList(kiraOff)

            monitorTasks = []
            for device in self.deviceList:
                if not device.type == "FC7":
                    self.executeDeviceCommand( device.index, "Start MonitorPoll with Saving", runInfo["Result_Folder"] + str(device.config["ID"])+ ".yml", None)
                    time.sleep(0.5)
                    monitorTasks.append(self.taskList[-1])

            if int(self.configurationList["CheckFW"]) >0:
                fc7 = [device for device in self.deviceList if device.type == "FC7"][0]
                module_type = slot.getModuleType()

                #To be removed during production
                if module_type == "2S":
                    try:
                        if slot.module.id[14] == "1":
                            module_type = "2S_FEC12"
                        else:
                            module_type = "2S_FEC5"
                    except:
                        module_type = "2S_FEC12"

                fw = [fw for fw in os.listdir(os.getcwd() + "/Bitstreams/") if module_type in fw][0]

                if fw != fc7.firmwareName:
                    if fw not in fc7.firmwareList:
                        uploadFW = UploadFirmware( len(self.taskList) , fc7 , fw)
                        self.extendTaskList(uploadFW)
                        checkFW = Check(len(self.taskList) , fc7, True) 
                        self.extendTaskList(checkFW)
                    switchFW = SwitchFirmware( len(self.taskList), fc7 , fw) 
                    self.extendTaskList(switchFW)

            for measurement in selectedMeasurements:
                if measurement == "IV":
                    #If on, for safety reasons, first ramp down HV, then LV, enable and then set the voltage
                    hvPowerSupplyAndChannelsAndZeroVoltages = []
                    lvPowerSupplyAndChannelsAndZeroVoltages = []
                    involvedPowerSuplliesAndChannels = []
                    lvPowerSupplyAndChannelsAndVoltages   = []

                    if not slot.module.skeleton:
                        hvComboZero = [slot.hvPowerSupply, slot.hvPowerSupplyChannel,0]
                        hvPowerSupplyAndChannelsAndZeroVoltages.append(hvComboZero)
                        lvZeroVoltage = 0
                        try:
                            lvZeroVoltage = self.powerSupplyModelDictionary[slot.lvPowerSupply.config["Model"]]["LVRange"][0]
                        except:
                            pass

                        lvComboZero = [slot.lvPowerSupply, slot.lvPowerSupplyChannel,lvZeroVoltage]
                        lvPowerSupplyAndChannelsAndZeroVoltages.append(lvComboZero)
                        involvedPowerSuplliesAndChannels.append(hvComboZero)
                        involvedPowerSuplliesAndChannels.append(lvComboZero)
                        lvCombo = [slot.lvPowerSupply, slot.lvPowerSupplyChannel,float ( testConditions["lvV"] )]#self.configurationList["LV_Test"] ) ]
                        lvPowerSupplyAndChannelsAndVoltages.append(lvCombo)


                    #First, ramp down HV independend what is on the PS
                    rampDownHv = SetVoltages(len(self.taskList), hvPowerSupplyAndChannelsAndZeroVoltages, False, True)
                    self.extendTaskList(rampDownHv)

                    if slot.lvPowerSupply.config["Model"] != "CAEN":
                        #Ramp down LV
                        rampDownLv = SetVoltages(len(self.taskList), lvPowerSupplyAndChannelsAndZeroVoltages, False, True)
                        self.extendTaskList(rampDownLv)

                        #Enabled LV and HV
                        enable = EnableChannels(len(self.taskList), involvedPowerSuplliesAndChannels, int(self.configurationList["PowerSupply_Dialog"]) > 0, True)
                        self.extendTaskList(enable)

                    #Set LV to 10.5V
                    setVoltage = SetVoltages(len(self.taskList), lvPowerSupplyAndChannelsAndVoltages, int(self.configurationList["PowerSupply_Dialog"]) > 0, True)
                    self.extendTaskList(setVoltage)

                    if slot.lvPowerSupply.config["Model"] == "CAEN":
                        #Enabled LV and HV
                        enable = EnableChannels(len(self.taskList), involvedPowerSuplliesAndChannels, int(self.configurationList["PowerSupply_Dialog"]) > 0, True)
                        self.extendTaskList(enable)

                    #After LV is disabled and HV enabled, do IV ramp on all modules
                    if not slot.module.skeleton and slot.hvPowerSupply is not None and slot.hvPowerSupplyChannel is not None:
                        if int(self.configurationList["VTRXLightOff"]) > 0:
                            lightOff = VTRXLightOff( len(self.taskList, ), slot, [device for device in self.deviceList if device.config["Type"] == "FC7"][0], self.configurationList)
                            self.extendTaskList(lightOff)

                        iv = ModuleIV( len(self.taskList), self.configurationList, testConditions, slot, runInfo)
                        self.extendTaskList(iv)

                    #After IV Ramp down HV
                    rampDownHv = SetVoltages(len(self.taskList), hvPowerSupplyAndChannelsAndZeroVoltages, False, True)
                    self.extendTaskList(rampDownHv)


                    if slot.lvPowerSupply.config["Model"] != "CAEN":
                        #Turn off module Ramp down LV
                        rampDownLv = SetVoltages(len(self.taskList), lvPowerSupplyAndChannelsAndZeroVoltages, False, True)
                        self.extendTaskList(rampDownLv)

                    disable = DisableChannels(len(self.taskList), involvedPowerSuplliesAndChannels, True)
                    self.extendTaskList(disable)

                #Offset(AndNoise) measurements come after the IV Measurements
                if measurement in ["Offset", "ECV"]:
                    #If on, for safety reasons, first ramp down HV, then LV, enable and then set the voltage
                    hvPowerSupplyAndChannelsAndZeroVoltages = []
                    lvPowerSupplyAndChannelsAndZeroVoltages = []
                    involvedPowerSuplliesAndChannels = []
                    lvPowerSupplyAndChannelsAndVoltages   = []
                    hvPowerSupplyAndChannelsAndVoltages   = []

                    if not slot.module.skeleton:
                        hvComboZero = [slot.hvPowerSupply, slot.hvPowerSupplyChannel,0]
                        hvPowerSupplyAndChannelsAndZeroVoltages.append(hvComboZero)
                        involvedPowerSuplliesAndChannels.append(hvComboZero)
                    lvZeroVoltage = 0
                    try:
                        lvZeroVoltage = self.powerSupplyModelDictionary[slot.lvPowerSupply.config["Model"]]["LVRange"][0]
                    except:
                        pass
                    lvComboZero = [slot.lvPowerSupply, slot.lvPowerSupplyChannel,lvZeroVoltage]

                    lvPowerSupplyAndChannelsAndZeroVoltages.append(lvComboZero)
                    involvedPowerSuplliesAndChannels.append(lvComboZero)
                    lvCombo = [slot.lvPowerSupply, slot.lvPowerSupplyChannel,float ( testConditions["lvV"] ) ]
                    lvPowerSupplyAndChannelsAndVoltages.append(lvCombo)
                    if not slot.module.skeleton:
                        hv = float(testConditions["hvNakedV"])
                        if int(slot.module.encapsulated) > 0:
                            hv = float(testConditions["hvEncapV"])
                        hvCombo = [slot.hvPowerSupply,slot.hvPowerSupplyChannel,float ( hv ) ] 
                        hvPowerSupplyAndChannelsAndVoltages.append(hvCombo)

                    #First, ramp down HV independend what is on the PS
                    rampDownHv = SetVoltages(len(self.taskList), hvPowerSupplyAndChannelsAndZeroVoltages, False, True)
                    self.extendTaskList(rampDownHv)

                    if slot.lvPowerSupply.config["Model"] != "CAEN":
                        #Ramp down LV
                        rampDownLv = SetVoltages(len(self.taskList), lvPowerSupplyAndChannelsAndZeroVoltages, False, True)
                        self.extendTaskList(rampDownLv)

                        #Enabled LV and HV
                        enable = EnableChannels(len(self.taskList), involvedPowerSuplliesAndChannels, int(self.configurationList["PowerSupply_Dialog"]) > 0, True)
                        self.extendTaskList(enable)

                    #Set LV to 10.5V
                    setLV = SetVoltages(len(self.taskList), lvPowerSupplyAndChannelsAndVoltages, int(self.configurationList["PowerSupply_Dialog"]) > 0, True)
                    self.extendTaskList(setLV)

                    if slot.lvPowerSupply.config["Model"] == "CAEN":
                        #Enabled LV and HV
                        enable = EnableChannels(len(self.taskList), involvedPowerSuplliesAndChannels, int(self.configurationList["PowerSupply_Dialog"]) > 0, True)
                        self.extendTaskList(enable)


                    #Set HV
                    setHV = SetVoltages(len(self.taskList), hvPowerSupplyAndChannelsAndVoltages, int(self.configurationList["PowerSupply_Dialog"]) > 0, True)
                    self.extendTaskList(setHV)

                    moduleTest  = ModuleTest( len(self.taskList), self.configurationList, self.deviceList, slot, runInfo, selectedMeasurements)
                    self.extendTaskList(moduleTest)

                    rampDownHv = SetVoltages(len(self.taskList), hvPowerSupplyAndChannelsAndZeroVoltages, False, True)
                    self.extendTaskList(rampDownHv)

                    if slot.lvPowerSupply.config["Model"] != "CAEN":
                        rampDownLv = SetVoltages(len(self.taskList), lvPowerSupplyAndChannelsAndZeroVoltages, False, True)
                        self.extendTaskList(rampDownLv)

                    disable = DisableChannels(len(self.taskList), involvedPowerSuplliesAndChannels, True)
                    self.extendTaskList(disable)

            monitorData = AnalyseMonitorData( len(self.taskList), slot, runInfo, monitorTasks )
            self.extendTaskList(monitorData)

            self.measurementSteps = len( self.taskList ) - self.measurementSteps
            self.measurementSteps = 1.0 / float(self.measurementSteps)
            self.window.updateRunProgressBar( 0 , "" )
            self.doNextTask( )
        else:
            #There are reasons to not start the measurement, show Dialog why
            print("GIPHT:\tMeasurement not started:")
            [print(self.CRED + reason + self.CEND) for reason in reasons]

    def askUser(self, pTask, pMessage):
        ok = QMessageBox.question(None, pMessage[0], pMessage[1], QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if ok == QMessageBox.Yes:
            pTask.permission = True
        else:
            self.stopTask(pTask)

    def executeTask ( self , pTask):
        newThread = QThread()
        self.threadList.append( newThread )
        pTask.taskDone.connect( self.processEndOfTask )
        pTask.askPermission.connect( self.askUser )     #Message Box to ask Permission (FW change or voltages)
        pTask.statusUpdate.connect( self.window.updateTaskProgressBar )
        pTask.progressUpdate.connect( self.window.updateTaskProgressBar )
        pTask.started.connect( pTask.run )
        pTask.moveToThread( newThread )
        pTask.start()
        
    def stopTask( self, pTask ):
        pTask.stopTask()

    def stopCurrentTask ( self ):
        for task in reversed ( self.taskList ):
            if task.running:
                self.stopTask(task)
                break

    def stopRun ( self ):
        [self.stopTask( task ) for task in self.taskList] # Stop all running tasks and marks the others as aborted

    #Searches for the next task that is not already done
    def getNextTask ( self ):
        for task in self.taskList:
            if not task.done and not task.running and not task.aborted:
                return task

    #Start the next task of the toplevel list taskList. Can be a measurement or a simple task
    def doNextTask( self ):
        nextTask = self.getNextTask()
        if nextTask is not None:
            self.executeTask(nextTask)
            self.window.updateRunProgressBar( self.window.ui.runProgressBar.value() / 100.0 , nextTask.type)
        else:
            #[task.running = False for task in self.taskList if task.type == "MonitorData" ]
            print("GIPHT:\t" + self.CGREEN + "Run finished" + self.CEND)
            self.window.updateRunProgressBar( 1 , "Run finished")

            for device in self.deviceList:
                if not device.type == "FC7":
                    self.executeDeviceCommand( device.index, "Stop MonitorPoll", None,None)



    #Called once a devices finished its task. Make an update on the GUI
    def processEndOfTask ( self , pTask ):
        pTask.statusUpdate.disconnect()
        pTask.progressUpdate.disconnect()
        #pTask.logUpdate.disconnect( )
        self.window.updateRunProgressBar( self.window.ui.runProgressBar.value() / 100.0 + self.measurementSteps , pTask.type)


        if pTask.type in self.measurements.keys() or pTask.type in ["ModuleTest", "ConditionData", "AnalyseMonitorData"]:
            newRunItem = pTask.generateRunItem()
            for run in self.runList:
                if run.runInfo["LocalRunNumber"] == newRunItem.runInfo["LocalRunNumber"]:
                    run.extendRunItems(newRunItem)#Dangerous! could add results to an old item which is loaded in between
                    break
            self.window.updateRuns(self.runList)


        if pTask.waitForTask and pTask.done:
            self.doNextTask()

    #-----------------------------------------------------------------------
    #-------------------------- Handle Results -----------------------------
    #-----------------------------------------------------------------------

    def loadResultFolder ( self ):
        forOpen = True
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog

        #options |= QFileDialog.DontUseCustomDirectoryIcons
        dialog = QFileDialog()
        dialog.setWindowTitle("Choose result folder" )
        dialog.setOptions(options)
        dialog.setFilter(dialog.filter() | QDir.Hidden)
        dialog.setFileMode(QFileDialog.Directory)
        dialog.setDirectory(os.path.dirname(self.configurationList["Result_Folder"]))
    
        # OPENING OR SAVING
        dialog.setAcceptMode(QFileDialog.AcceptOpen) if forOpen else dialog.setAcceptMode(QFileDialog.AcceptSave)

        if dialog.exec_() == QDialog.Accepted:
            path = dialog.selectedFiles()[0]  # returns a list
            newRunFromFolder = Run()
            if newRunFromFolder.generateRunDataFromFolder(path):
                existent = False
                for run in self.runList:
                    if run.runInfo["LocalRunNumber"] == newRunFromFolder.runInfo["LocalRunNumber"]:
                        existent = True
                if not existent:
                    self.runList.append(newRunFromFolder)
                    self.window.updateRuns(self.runList) 
                else:
                    print("GIPHT:\tRun already loaded in run list!")
            else:
                print("GIPHT:\tCould not create run from this folder")

    #Opens various file types, csv root etc.
    def resultTreeClicked( self , pItem = None, pColumn = None):
        try:
            directory = pItem.parent().text(4)
            if pColumn == 6:#Edit comment
                #Open a dialog to edit the comment line 
                comment = self.askForComment(pItem.text(6))
                localRunNumber = pItem.parent().text(0)
                fileName = pItem.text(4)
                #Search for the corresponding result object 
                for run in self.runList:
                    #print(int( localRunNumber ))
                    for runItem in run.runItems:
                        for dataFile in runItem.dataFiles:
                            if run.runInfo["LocalRunNumber"] == int( localRunNumber ) and dataFile["Uploadable"] and dataFile["FileName"] == fileName:
                                dataFile["Comment"] = comment
                                run.updateRunItems()
                self.window.updateRuns(self.runList)
                
            elif pColumn in [4,5]:#Open File(s)
                file = pItem.text(pColumn)
                
                fileType = file.split(".")[-1]
                measurementType = file.split("_")[0]
                filePath = directory + file

                print(f"GIPHT:\tOpen file  " + filePath)
                if fileType == "root":
                    self.tbrowser = subprocess.Popen( [ "root", "-l","--web=off" , filePath, "-e", "TBrowser x"] ,  stdout=subprocess.PIPE, stderr=subprocess.PIPE  )
                elif fileType == "yml":
                    if measurementType == "Monitor":
                        plot = MonitorPlot(filePath, "Environment")
                        plot2 = MonitorPlot(filePath, "HV")
                        plot3 = MonitorPlot(filePath, "LV")
                    elif measurementType == "IV":
                        subprocess.Popen( ["gedit", os.path.abspath(filePath)],  stdout=subprocess.PIPE, stderr=subprocess.PIPE )
                        plot = IvPlot(filePath, True)
                    else:
                        subprocess.Popen( ["gedit", os.path.abspath(filePath)],  stdout=subprocess.PIPE, stderr=subprocess.PIPE )
                elif fileType in ["xml","txt","csv"]:
                        subprocess.Popen( ["gedit", os.path.abspath(filePath)],  stdout=subprocess.PIPE, stderr=subprocess.PIPE )
                elif fileType == "png":
                    subprocess.Popen( [ "xdg-open", filePath],  stdout=subprocess.PIPE, stderr=subprocess.PIPE )

        except AttributeError:        # Open Folder
            subprocess.Popen( [ "gio", "open" , pItem.text(4)] ,  stdout=subprocess.PIPE, stderr=subprocess.PIPE  )

    def createCSVandZIPFiles ( self ):
        if self.db is None:
            print("GIPHT:\tDB not initialized, cannot convert!")
            return
        for i in range(self.window.ui.resultsTreeWidget.topLevelItemCount()) :
            #Get checked runs
            if self.window.ui.resultsTreeWidget.topLevelItem(i).checkState(0) == Qt.Checked:
                runNumber = int ( self.window.ui.resultsTreeWidget.topLevelItem(i).text(0) )
                directory = self.window.ui.resultsTreeWidget.topLevelItem(i).text(4)
                print("GIPHT:\tSearch for convertable files in run with runnumber " + str(runNumber))
                #Get the corresponding run
                for run in [item for item in self.runList if item.runInfo["LocalRunNumber"] == runNumber] :
                    #Loop over run items
                    for runItem in run.runItems:
                        #Loop over data files
                        for dataFile in [file for file in runItem.dataFiles if not file["Uploaded"]]:
                            if "IV" in dataFile["FileName"] and ".yml" in dataFile["FileName"]:
                                self.db.createModuleIVCSV(run, directory, dataFile)
                            elif ".root" in dataFile["FileName"]:
                                summaryFile = [file["FileName"] for file in runItem.dataFiles if "ModuleTest" == file["FileName"][:10]][0]
                                print(summaryFile)
                                self.db.createModuleTestCSV(run, summaryFile, directory, dataFile)
                        run.updateRunItems()
        self.window.updateRuns(self.runList)

    def uploadCSVFilesToDb( self ):
        #Loop over runs /top level items
        for i in range(self.window.ui.resultsTreeWidget.topLevelItemCount()) :
            #Get checked runs
            if self.window.ui.resultsTreeWidget.topLevelItem(i).checkState(0) == Qt.Checked:
                runNumber = int ( self.window.ui.resultsTreeWidget.topLevelItem(i).text(0) )
                directory = self.window.ui.resultsTreeWidget.topLevelItem(i).text(4)
                print("GIPHT:\tSearch for convertable files in run with runnumber " + str(runNumber))
                #Get the corresponding run
                for run in [item for item in self.runList if item.runInfo["LocalRunNumber"] == runNumber] :
                    #Loop over run items
                    for runItem in run.runItems:
                        #Loop over datafils
                        for dataFile in [file for file in runItem.dataFiles if not file["Uploaded"] and file["DBFile"] != ""]:
                            print("GIPHT:\tUpload file " + dataFile["DBFile"] + " to CMS construction database")
                            response = self.db.uploadFile(os.path.abspath(directory + dataFile["DBFile"]))
                            if response == 200:
                                print("GIPHT:\tUpload successful")
                                dataFile["Uploaded"] = True
                            else:
                                print("GIPHT:\tUpload unsuccessful")
                                dataFile["Uploaded"] = False

                    run.updateRunItems()
        self.window.updateRuns(self.runList)


    def convertAllResultFilesToXml ( self ):
        pass

    def uploadAllXmlFilesToDb( self ):
        pass

    def askForComment ( self , pComment = "" ):
        comment, ok = QInputDialog.getText(None, 'Comment', 'Enter Comment', text = pComment)
        if ok:
            return comment
        else:
            return pComment

    #-----------------------------------------------------------------------
    #--------------------------------- CLOSE -------------------------------
    #-----------------------------------------------------------------------

    #Called when window is closed to avoid unclosed root tbrowsers screwing everything up
    def cleanUp(self):
        #Make sure root is not up running
        if self.tbrowser:
            self.tbrowser.terminate()

        #Remove all temporary Device and Measurement XML files
        print("GIPHT:\tRemove temporary device files")
        directory = os.getcwd() + "/settings/Devices/"
        filelist = [ f for f in os.listdir(directory) if f.endswith(".xml") ]
        for f in filelist:
            os.remove(os.path.join(directory, f))
        print("GIPHT:\tRemove temporary measurement files")
        directory = os.getcwd() + "/settings/Measurements/"
        filelist = [ f for f in os.listdir(directory) if f.endswith(".xml") ]
        for f in filelist:
            os.remove(os.path.join(directory, f))
        print("GIPHT:\tRemove temporary task files")
        directory = os.getcwd() + "/settings/Tasks/"
        filelist = [ f for f in os.listdir(directory) if f.endswith(".xml") ]
        for f in filelist:
            os.remove(os.path.join(directory, f))



        if os.path.isfile(os.getcwd() + "/logs/timestamps.yml"):
            os.remove(os.getcwd() + "/logs/timestamps.yml")


    #-----------------------------------------------------------------------
    #--------------------------------- UTILS -------------------------------
    #-----------------------------------------------------------------------

    def startControlhub(self):
        print("GIPHT:\tTry to start controlhub in default installation folder /opt/cactus/bin/controlhub_start")
        try:
            check = subprocess.Popen('sh /opt/cactus/bin/controlhub_start' ,stdout=subprocess.PIPE, stderr=subprocess.PIPE, executable="/bin/bash", shell=True) 
        except:
            print("GIPHT:\tControlhub could not be started")
            return
        #Get all variables and count how much they are present in the list
        returnstring = check.communicate()[0].decode('utf-8')
        if "Starting ControlHub ... ok" in returnstring:
            print("GIPHT:\tControlhub started")
            if self.checkIfControlhubIsRunning():
                self.checkIfPh2AcfIsResponsive()
        elif "ControlHub is already running!" in returnstring:
            print("GIPHT:\tControlhub is already running")
        else:
            print("GIPHT:\tControlhub could not be started - Please do manually")


    def setEnvironmentVariablesForPh2ACF(self):
        #get environment variables , source Ph2_ACF, get environment variables again. Everything in one subprocess
        try:
            check = subprocess.Popen('printenv && source ./setup.sh && printenv' ,stdout=subprocess.PIPE, stderr=subprocess.PIPE, executable="/bin/bash", shell=True, cwd=self.configurationList["Ph2_ACF_Folder"]) 
        except FileNotFoundError:
            print("GIPHT:\tPh2_ACF folder not exist!")
            return
        #Get all variables and count how much they are present in the list
        env_variables = check.communicate()[0].splitlines()
        #Split the entries and set them for the parent process
        #Skip the BASH_FUNC blocks
        for entry in env_variables:
            if entry.decode().startswith(' ') or entry.decode().startswith('}') or entry.decode().startswith('BASH_FUNC'):
                continue
            #print(entry.decode())
            try:
                splitEntry = entry.decode().split('=')
                os.environ[splitEntry[0]] = splitEntry[1]
            except:
                continue

        #sys.path.insert(1, os.getenv('PH2ACF_BASE_DIR'))
        print("GIPHT:\tEnvironment variables for Ph2_ACF set")


    def compileAndUploadArduinoSoftware( self, pPort ):
        print("GIPHT:\tCompile and upload most current software for module test bench arduino: " + pPort)
        self.stopDevices()
        self.window.setGiphtBusy(True)
        self.deviceList.clear()
        self.thread = QThread(parent = self )
        # Step 3: Create a worker object
        self.worker = UploadThread(pPort)
        # Step 4: Move worker to the thread
        self.worker.moveToThread(self.thread)

        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(partial(self.startDevices,True))
        self.worker.finished.connect(partial(self.window.setGiphtBusy,False))
        self.thread.start()

    #-----------------------------------------------------------------------
    #--------------------------------- TEST --------------------------------
    #-----------------------------------------------------------------------

    def test (self):
        #from py4dbupload.modules.Query import Query
        #query = Query()
        self.db.getRecentTestConditions()

class UploadThread(QObject):
    finished = Signal()
    def __init__(self, pPort):
        super(UploadThread, self).__init__(  )
        self.port = pPort

    def run ( self ):
        time.sleep(1)
        os.system("./arduinoUpdate.sh " + self.port)
        time.sleep(1)
        self.finished.emit()
