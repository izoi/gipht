#------------------------------------------------
#+++++++++++++Slot.py++++++++++++++++++++++
#Created by Stefan Maier            10.07.2020
#Last modified by Stefan Maier      21.04.2022
#------------------------------------------------
from Dialog.SlotStatusWidget import SlotStatusWidget
from PySide2.QtWidgets import QWidget
from PySide2.QtCore import Signal, Qt
from src.Module import Module

class Slot(QWidget):
    newModule = Signal( object )

    def __init__( self, pIndex, pDevices = None ):
        super(Slot, self).__init__()
        self.index                  = pIndex
        self.active                 = pIndex == 0 #Slot can be activate/deactivated
        self.module                 = Module()  #Stores the module object
        self.arduino                = None
        self.lvPowerSupply          = None
        self.lvPowerSupplyChannel   = None
        self.hvPowerSupply          = None
        self.hvPowerSupplyChannel   = None

        #Generate the UI for the slot to display the current status of the powersupplies etc
        self.statusWidget = SlotStatusWidget( self )
        self.statusWidget.ui.skeletonCheckBox.setEnabled(False)
        self.statusWidget.ui.encapsulatedCheckBox.setEnabled(False)
        self.statusWidget.ui.slotCheckBox.stateChanged.connect(self.stateChanged)
        self.statusWidget.ui.slotCheckBox.setChecked(self.active)
        self.statusWidget.ui.moduleTypeComboBox.setDisabled(True)
        self.statusWidget.moduleInput.connect(self.newModuleInput)

        self.devices = pDevices

        self.updateDeviceSignals()
    def updateDeviceSignals( self ):
        if self.devices is not None:
            self.lvPowerSupply          = self.devices["LV_PowerSupply"]
            self.lvPowerSupplyChannel   = self.devices["LV_Channel"]
            self.hvPowerSupply          = self.devices["HV_PowerSupply"]
            self.hvPowerSupplyChannel   = self.devices["HV_Channel"]
            self.arduino                = self.devices["Arduino"]

            #Print for the slot config table
            line = "\t" + str(self.index)
            if self.lvPowerSupply is not None:
                self.lvPowerSupply.statusUpdate.connect(self.statusWidget.updateStatus)
                line += "\t" + self.lvPowerSupply.config["ID"]
            else:
                line += "\t-"
            if self.hvPowerSupply is not None:
                self.hvPowerSupply.statusUpdate.connect(self.statusWidget.updateStatus)
                line += "\t" + self.hvPowerSupply.config["ID"]
            else:
                line += "\t-"
            if self.arduino is not None:
                self.arduino.statusUpdate.connect(self.statusWidget.updateStatus)
                line += "\t" + self.arduino.config["ID"]
            else:
                line += "\t-"
            #print(line)

    #Changes the active state of the Slot
    def stateChanged(self , pState):
        self.active = pState

    #Tells the controller there is a new module entry to check
    def newModuleInput( self ):
        self.newModule.emit(self.index)

    def getModuleIdText( self ):
        return self.statusWidget.ui.moduleIdLineEdit.text()

    def getModuleType( self ):
        return self.statusWidget.ui.moduleTypeComboBox.currentText()

    def setModuleTextColor( self, pColor ):
        self.statusWidget.ui.moduleIdLineEdit.setStyleSheet("background-color: " + pColor)

    #Called when the slot information must be written to disk
    def getSlotDictionary( self ):
        returnDict = {}
        returnDict["Index"]     = self.index
        returnDict["Module_ID"]         = self.module.id                            if self.module                  is not None else ""
        returnDict["Arduino"]           = self.arduino.config["ID"]                 if self.arduino                 is not None else ""
        returnDict["LV_PowerSupply"]    = self.lvPowerSupply.config["ID"]           if self.lvPowerSupply           is not None else ""
        returnDict["LV_Channel"]        = self.lvPowerSupplyChannel.config["ID"]    if self.lvPowerSupplyChannel    is not None else ""
        returnDict["HV_PowerSupply"]    = self.hvPowerSupply.config["ID"]           if self.hvPowerSupply           is not None else ""
        returnDict["HV_Channel"]        = self.hvPowerSupplyChannel.config["ID"]    if self.hvPowerSupplyChannel    is not None else ""

        return returnDict
